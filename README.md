# eslint-plugin-typescript-extended-rules

Extended typescript rules for ESLint

## Installation

You'll first need to install [ESLint](https://eslint.org/):

```sh
npm i -D eslint
```

Next, install `eslint-plugin-typescript-extended-rules`:

```sh
npm install -D eslint-plugin-typescript-extended-rules
```

## Usage

Add `typescript-extended-rules` to the plugins section of your `.eslintrc` configuration file. You can omit the `eslint-plugin-` prefix:

```json
{
  "plugins": ["typescript-extended-rules"]
}
```

Then configure the rules you want to use under the rules section.

```json
{
  "rules": {
    "typescript-extended-rules/file-naming-convention": [
      "error",
      {
        allowedCases: ["kebab-case"],
      },
    ],
    "typescript-extended-rules/max-declarations-per-file": [
      "error",
      {
        "maxDeclarations": 1
      }
    ],
    "typescript-extended-rules/no-empty-return": "error",
    "typescript-extended-rules/prefer-static-method": ["error",  {"exceptMethods": ["foo"], "exceptFilenamesPattern": [/\.bar\./, /^zoo$/]}],
  }
}
```

## Rules

<!-- begin auto-generated rules list -->

⚠️ If you are in [npmjs.com](https://www.npmjs.com) site, then use [this link](https://gitlab.com/btd1337/eslint-plugin-typescript-extended-rules#rules) to see the rules.

| Name                                                                 | Description                                                                                |
| :------------------------------------------------------------------- | :----------------------------------------------------------------------------------------- |
| [file-naming-convention](docs/rules/file-naming-convention.md)       | Validates if the file name follows the allowed cases                                       |
| [max-declarations-per-file](docs/rules/max-declarations-per-file.md) | Declare the maximum classes, interfaces, enums, and types or the sum of all these per file |
| [no-empty-return](docs/rules/no-empty-return.md)                     | Avoid empty returns (return;)                                                              |
| [prefer-static-method](docs/rules/prefer-static-method.md)           | Use static methods in functions that do not access the global properties of a class        |

<!-- end auto-generated rules list -->
