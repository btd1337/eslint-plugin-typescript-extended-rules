/**
 * @fileoverview Declare the maximum classes, interfaces, enums and types per file
 * @author Helder
 */
"use strict";

//------------------------------------------------------------------------------
// Rule Definition
//------------------------------------------------------------------------------

/** @type {import('eslint').Rule.RuleModule} */
module.exports = {
	meta: {
		type: "layout",
		docs: {
			description:
				"Declare the maximum classes, interfaces, enums, and types or the sum of all these per file",
			recommended: false,
			url: null,
		},
		fixable: null,
		schema: [
			{
				type: "object",
				properties: {
					maxClasses: {
						type: "integer",
						minimum: 0,
					},
					maxDeclarations: {
						type: "integer",
						minimum: 0,
					},
					maxEnums: {
						type: "integer",
						minimum: 0,
					},
					maxInterfaces: {
						type: "integer",
						minimum: 0,
					},
					maxTypes: {
						type: "integer",
						minimum: 0,
					},
				},
				additionalProperties: false,
			},
		],
		messages: {
			exceededMaxClasses:
				"File has too many classes ({{classCount}}). Maximum allowed is {{maxClasses}}.",
			exceededMaxDeclarations:
				"File has too many declarations (classes, enums, interfaces and types) ({{declarationsCount}}). Maximum allowed is {{maxDeclarations}}.",
			exceededMaxEnums:
				"File has too many enums ({{enumCount}}). Maximum allowed is {{maxEnums}}.",
			exceededMaxInterfaces:
				"File has too many interfaces ({{interfaceCount}}). Maximum allowed is {{maxInterfaces}}.",
			exceededMaxTypes:
				"File has too many types ({{typeCount}}). Maximum allowed is {{maxTypes}}.",
		},
	},

	create(context) {
		const {
			maxClasses = Infinity,
			maxDeclarations = Infinity,
			maxEnums = Infinity,
			maxInterfaces = Infinity,
			maxTypes = Infinity,
		} = context.options[0] || {};
		let classCount = 0;
		let declarationsCount = 0;
		let enumCount = 0;
		let interfaceCount = 0;
		let typeCount = 0;

		return {
			ClassDeclaration() {
				classCount++;
			},
			TSInterfaceDeclaration() {
				interfaceCount++;
			},
			TSEnumDeclaration() {
				enumCount++;
			},
			TSTypeAliasDeclaration() {
				typeCount++;
			},
			"Program:exit"() {
				const filePath = context.getFilename();
				declarationsCount =
					classCount + enumCount + interfaceCount + typeCount;

				if (declarationsCount > maxDeclarations) {
					context.report({
						loc: { line: 1, column: 0 },
						messageId: "exceededMaxDeclarations",
						data: {
							declarationsCount,
							maxDeclarations,
						},
						filePath,
					});
				}

				if (classCount > maxClasses) {
					context.report({
						loc: { line: 1, column: 0 },
						messageId: "exceededMaxClasses",
						data: {
							classCount,
							maxClasses,
						},
						filePath,
					});
				}

				if (enumCount > maxEnums) {
					context.report({
						loc: { line: 1, column: 0 },
						messageId: "exceededMaxEnums",
						data: {
							enumCount,
							maxEnums,
						},
						filePath,
					});
				}

				if (interfaceCount > maxInterfaces) {
					context.report({
						loc: { line: 1, column: 0 },
						messageId: "exceededMaxInterfaces",
						data: {
							interfaceCount,
							maxInterfaces,
						},
						filePath,
					});
				}

				if (typeCount > maxTypes) {
					context.report({
						loc: { line: 1, column: 0 },
						messageId: "exceededMaxTypes",
						data: {
							typeCount,
							maxTypes,
						},
						filePath,
					});
				}
			},
		};
	},
};
