/**
 * @fileoverview Checks if functions in a class use "this" and suggests converting to static if not used.
 * @author Your Name
 */
"use strict";

const path = require("path");

//------------------------------------------------------------------------------
// Rule Definition
//------------------------------------------------------------------------------

/** @type {import('eslint').Rule.RuleModule} */
module.exports = {
  meta: {
    type: "suggestion",
    docs: {
      description:
        "Checks if functions in a class use 'this' and suggests converting to static if not used.",
      recommended: false,
      url: null,
    },
    fixable: null,
    schema: [
      {
        type: "object",
        properties: {
          exceptMethods: {
            type: "array",
            items: {
              type: "string",
            },
          },
          exceptFilenamesPattern: {
            type: "array",
            items: {
              type: "regex",
            },
            uniqueItems: true,
          },
        },
        additionalProperties: false,
      },
    ],
    messages: {
      preferStaticMethod:
        "Consider converting the method '{{methodName}}' to static, as it does not use 'this'.",
    },
  },

  create(context) {
    let usesThis = false;
    let isStaticMethod = false;

    const options = context.options[0] || {};
    const exceptMethods = options.exceptMethods || [];
    let exceptFilenamesPattern = options.exceptFilenamesPattern || [];

    const filePath = context.getFilename();
    const fileName = path.basename(filePath);
    const fileNameWithoutExtension = path.parse(fileName).name;

    // Ignore if the file is listed in exceptions
    if (
      exceptFilenamesPattern.some((exception) =>
        exception.test(fileNameWithoutExtension)
      )
    ) {
      return {};
    }

    // Reset flags at the beginning of each class
    usesThis = false;
    isStaticMethod = false;

    return {
      MethodDefinition(node) {
        const methodName = node.key.name;
        isStaticMethod = node.static;

        // Ignore if the method is a constructor or listed in exceptions
        if (
          methodName === "constructor" ||
          exceptMethods.includes(methodName)
        ) {
          return;
        }

        if (!isStaticMethod) {
          // Check if the function uses "this"
          usesThis = false;

          // Iterate over the statements within the method
          node.value.body.body.forEach((statement) => {
            if (
              statement.type === "ExpressionStatement" &&
              statement.expression.type === "AssignmentExpression" &&
              statement.expression.left.type === "MemberExpression" &&
              statement.expression.left.object.type === "ThisExpression"
            ) {
              // If there is an assignment using "this", mark as using "this"
              usesThis = true;
            }
          });

          // If not using "this", suggest conversion to static
          if (!usesThis) {
            context.report({
              node,
              messageId: "preferStaticMethod",
              data: {
                methodName,
              },
            });
          }
        }
      },

      // Reset the flag after processing each method
      "MethodDefinition:exit"(node) {
        if (node.static) {
          isStaticMethod = false;
        }
      },
    };
  },
};
