module.exports = {
  meta: {
    type: 'suggestion',
    docs: {
      description: 'Disallow empty return statements',
      category: 'Stylistic Issues',
      recommended: true,
    },
    schema: [],
  },

  create(context) {
    return {
      ReturnStatement(node) {
        if (!node.argument) {
          context.report({
            node,
            message: 'Empty return statement is not allowed.',
          });
        }
      },
    };
  },
};
