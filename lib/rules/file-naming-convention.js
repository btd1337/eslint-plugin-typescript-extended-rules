/**
 * @fileoverview Validates if the file name follows the allowed cases
 * @author Helder Reis
 */
"use strict";

//------------------------------------------------------------------------------
// Rule Definition
//------------------------------------------------------------------------------

/** @type {import('eslint').Rule.RuleModule} */
module.exports = {
	meta: {
		type: "layout",
		docs: {
			description: "Validates if the file name follows the allowed cases",
			recommended: false,
			url: null,
		},
		fixable: null,
		schema: [
			{
				type: "object",
				properties: {
					allowedCases: {
						type: "array",
						items: {
							enum: [
								"camelCase",
								"strictCamelCase",
								"kebab-case",
								"PascalCase",
								"StrictPascalCase",
								"snake_case",
								"UPPER_CASE",
							],
						},
						minItems: 1,
						uniqueItems: true,
					},
				},
				additionalProperties: false,
			},
		],
		messages: {
			invalidFileName:
				"File name does not follow the allowed cases: {{allowedCases}}",
		},
	},

	create(context) {
		const { allowedCases } = context.options[0];

		//----------------------------------------------------------------------
		// Helpers
		//----------------------------------------------------------------------

		function validateFileName(fileName) {
			const nameWithoutExtension = fileName.split(".")[0];
			const caseRegexMap = {
				snake_case: /^[a-z0-9_]+$/,
				"kebab-case": /^[a-z0-9-]+$/,
				PascalCase: /^[A-Z][a-zA-Z0-9]*$/,
				camelCase: /^[a-z][a-zA-Z0-9]*$/,
				strictCamelCase: /^[a-z][a-zA-Z0-9]*$/,
				StrictPascalCase: /^[A-Z][a-zA-Z0-9]*$/,
				UPPER_CASE: /^[A-Z0-9_]+$/,
			};

			for (const caseType of allowedCases) {
				const caseRegex = caseRegexMap[caseType];
				if (caseRegex.test(nameWithoutExtension)) {
					return; // File name matches the allowed case
				}
			}

			// File name does not match any allowed case, report an error
			context.report({
				messageId: "invalidFileName",
				data: {
					allowedCases: allowedCases.join(", "),
				},
				loc: {
					line: 1,
					column: 0,
				},
			});
		}

		//----------------------------------------------------------------------
		// Public
		//----------------------------------------------------------------------

		return {
			Program() {
				const filename = context.getFilename();
				const fileName = filename.split("/").pop();
				validateFileName(fileName);
			},
		};
	},
};
