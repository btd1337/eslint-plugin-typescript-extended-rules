# Check if Methods in a Class Use "this" and Suggest Conversion to Static if Unused (`typescript-extended-rules/prefer-static-method`)

<!-- end auto-generated rule header -->

This ESLint rule checks if methods in a class use the "this" keyword and suggests converting them to static methods if the "this" keyword is not used.

## Rule Details

This rule aims to improve code quality by encouraging the use of static methods when the "this" keyword is not necessary within class methods.

### Examples of **incorrect** code for this rule:

```ts
class Example {
  // Incorrect: This method does not use 'this' and should be static.
  regularMethod() {
    someProperty = 42;
  }
}
```

### Examples of correct code for this rule:

```ts
class Example {
  // Correct: This method uses 'this' and is appropriate as an instance method.
  regularMethod() {
    this.someProperty = 42;
  }

  // Correct: This method is static and does not use 'this'.
  static staticMethod() {
    // implementation
  }
}
```

### Options

This rule supports the following options:

#### Validate all declarations at once

```json
{
    "rules": {
        "eslint-plugin-example-rule":
        [
            'error',
            {
                exceptMethods: ["foo"],
                exceptFilenamesPattern: [/\.bar\./,/^zoo$/],
            },
        ]
    }
}
```

## When Not To Use It

It might be appropriate to turn off this rule if the use of "this" in class methods is intentional and desired, or if the codebase follows a different set of conventions.
