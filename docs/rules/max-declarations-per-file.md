# Declare the maximum classes, interfaces, enums, and types or the sum of all these per file (`typescript-extended-rules/max-declarations-per-file`)

<!-- end auto-generated rule header -->

Please describe the origin of the rule here.

## Rule Details

This rule aims to...

Examples of **incorrect** code for this rule:

```js

// fill me in

```

Examples of **correct** code for this rule:

```js

// fill me in

```

### Options

If there are any options, describe them here. Otherwise, delete this section.

#### Validate all declarations at once

```json
{
    "rules": {
        "typescript-extended-rules/max-declarations-per-file": 
        [
            'error', 
            {
                maxDeclarations: 1,
            },
        ]
    }
}
```
#### Validate individually

```json
{
    "rules": {
        "typescript-extended-rules/max-declarations-per-file": 
        [
            'error', 
            {
                maxClasses: 1,
                maxEnums: 1,
                maxInterfaces: 1,
                maxTypes: 1,
            },
        ]
    }
}
```

## When Not To Use It

Give a short description of when it would be appropriate to turn off this rule.

## Further Reading

If there are other links that describe the issue this rule addresses, please include them here in a bulleted list.
