# Validates if the file name follows the allowed cases (`typescript-extended-rules/file-naming-convention`)

<!-- end auto-generated rule header -->

Please describe the origin of the rule here.

## Rule Details

```js
'typescript-extended-rules/file-naming-convention': [
      'warn',
      {
        allowedCases: ['kebab-case'],
      },
    ],
```

Examples of **incorrect** code for this rule:

```js

FileName: FileManager.ts

```

Examples of **correct** code for this rule:

```js

FileName: file-manager.ts

```

### Options

- `allowedCases`: camelCase, strictCamelCase, kebab-case, PascalCase, StrictPascalCase, snake_case, UPPER_CASE

## When Not To Use It

Give a short description of when it would be appropriate to turn off this rule.

## Further Reading

If there are other links that describe the issue this rule addresses, please include them here in a bulleted list.
