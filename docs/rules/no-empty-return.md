# ESLint Rule: (`typescript-extended-rules/no-empty-return`)

This ESLint rule disallows the use of empty return statements (`return;`) in functions.

## Rule Details

This rule helps enforce consistent coding styles by preventing the use of empty return statements. An empty return statement adds no value to the code and can be safely removed.

Examples of **incorrect** code for this rule:

```ts
function emptyReturn() {
  return;
}
```

Examples of correct code for this rule:

```ts
function validReturn() {
  return 42;
}

function noReturn() {
  // No return statement
}
```

## Options

This rule does not have any configuration options.

## When Not To Use It

You should not use this rule if you do not want to enforce a coding style that disallows empty return statements. In some cases, empty return statements may be used intentionally for code readability or other reasons.

## Further Reading

ESLint Documentation

## Related Rules

eslint-plugin-eslint-plugin: The rule uses this ESLint plugin to define custom ESLint rules.

## License

This ESLint rule is licensed under the ISC License.
