/**
 * @fileoverview Declare the maximum classes, interfaces, enums and types per file
 * @author Helder
 */
"use strict";

//------------------------------------------------------------------------------
// Requirements
//------------------------------------------------------------------------------

const rule = require("../../../lib/rules/max-declarations-per-file"),
  RuleTester = require("eslint").RuleTester;


//------------------------------------------------------------------------------
// Tests
//------------------------------------------------------------------------------

const ruleTester = new RuleTester();
ruleTester.run("max-declarations-per-file", rule, {
  valid: [
    // give me some code that won't trigger a warning
  ],

  invalid: [
    {
      code: "código que vai falhar",
      errors: [{ message: "Fill me in.", type: "Me too" }],
    },
  ],
});
