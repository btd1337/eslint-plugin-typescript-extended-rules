/**
 * @fileoverview Validates if the file name follows the allowed cases
 * @author Helder Reis
 */
"use strict";

//------------------------------------------------------------------------------
// Requirements
//------------------------------------------------------------------------------

const rule = require("../../../lib/rules/file-naming-convention"),
  RuleTester = require("eslint").RuleTester;


//------------------------------------------------------------------------------
// Tests
//------------------------------------------------------------------------------

const ruleTester = new RuleTester();
ruleTester.run("file-naming-convention", rule, {
  valid: [
    // give me some code that won't trigger a warning
  ],

  invalid: [
    {
      code: "Example of code that fail",
      errors: [{ message: "Fill me in.", type: "Me too" }],
    },
  ],
});
