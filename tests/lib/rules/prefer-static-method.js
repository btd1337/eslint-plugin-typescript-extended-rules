/**
 * @fileoverview Validates if the file name follows the allowed cases
 * @author Helder Reis
 */
"use strict";

//------------------------------------------------------------------------------
// Requirements
//------------------------------------------------------------------------------

const RuleTester = require("eslint").RuleTester;

//------------------------------------------------------------------------------
// Tests
//------------------------------------------------------------------------------

const ruleTester = new RuleTester();
const rule = require("../../../lib/rules/prefer-static-method");

ruleTester.run("prefer-static-method", rule, {
  valid: [
    // Provide valid code examples that won't trigger a warning
    // Example: { code: "const myVariable = 42;", filename: "validFileName.js" },
  ],

  invalid: [
    {
      code: "Example of code that fail",
      errors: [{ message: "Fill me in.", type: "Me too" }],
    },
  ],
});
